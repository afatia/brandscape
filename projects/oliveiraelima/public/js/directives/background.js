define(['./module'], function (directives) {
  'use strict';

directives.directive('background', function() {
    return {
            restrict: "C",

            controller: function ($timeout) {

              function checkheight () {
                  var wrapper = $('.wrapper').height();
                  var container = $('.background').height();
                  var body = $('[ui-view="body"]').height();

                  if (wrapper < body) {
                    $('.background').css('height', wrapper + (body-wrapper));
                  }
                  else {
                    $('.background').css('height', wrapper);
                  }

                  $timeout(checkheight, 100);
                }

                checkheight();
              }
        };
    });
});
