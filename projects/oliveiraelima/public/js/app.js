define([
    'angular',
    'angular-route',
    'angular-sanitize',
    'angular-animate',
    'lodash',
    'restangular',
    'bootstrap-js',
    'angular-ui-router',
    './services/index',
    './controllers/index',
    './directives/index',
    './animations/index'
    // './filters/index',
  ], function (angular) {
      'use strict';

        return angular.module('app', [
            'app.services',
            'app.directives',
            'app.controllers',
            'app.animations',
            // 'app.filters',
            'ui.router',
            'restangular',
            'ngRoute',
            'ngSanitize',
            'ngAnimate'
        ]).constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
  });
