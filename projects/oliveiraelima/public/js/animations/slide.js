define(['./module'], function (animations) {
    'use strict';
    animations.animation('.slide-animation', function() {
      return {
      beforeAddClass: function (element, className, done) {
        if (className == 'toggle'){
            TweenMax.to(element, 1, {
              opacity:0.5
            });
        }
        else {
          done();
        }
      },
      beforeRemoveClass: function (element, className, done) {
        if (className == 'toggle') {
          TweenMax.to(element, 1, {
            opacity:1
          });
        }
        else {
          done();
        }
      }
    };
    });
});
