define(['./module'], function (controllers) {

  controllers.controller('homeController', function($timeout, $state, $scope) {

    // $state.transitionTo('home.premium');

    var slides = [
         {state: 'home.construcao', time: 6000},
         {state: 'home.business', time: 4000},
         {state: 'home.premium', time: 4000},
         {state: 'home.saude', time: 4000}
     ];

    var index = 0;
    var promise;

    var slide = function() {
      $state.transitionTo(slides[index].state);
      promise = $timeout(slide, slides[index].time);
      index = (index < slides.length - 1) ? index + 1 : 0;
    };

    slide();

    $scope.$on('$locationChangeStart', function(){
       $timeout.cancel(promise);
     });

    });
});
