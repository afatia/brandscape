define(['./module'], function (controllers) {

  controllers.controller('navbarController', function($scope, $location) {

      $scope.isActive = function (viewLocation) {

        var path = $location.path().split("/")[1];
        return viewLocation === path;
    };

  });

});
