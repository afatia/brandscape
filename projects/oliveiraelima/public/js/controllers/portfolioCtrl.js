define(['./module'], function (controllers) {

  controllers.controller('portfolioController', function($scope, imageService, $stateParams) {

    var images = imageService.getPortfolio();

    $scope.imgValue = ($stateParams.imageId - 1);

    $scope.navBack = true;
    $scope.navForward = true;

    images.then(function(images) {

      if ($scope.imgValue === 0 || $scope.imgValue < 0 || $scope.imgValue > _.size(images) - 1 ) {
          $scope.navBack = false;
      }

      if ($scope.imgValue === _.size(images) - 1) {
          $scope.navForward = false;
      }

      if ($scope.imgValue > _.size(images) - 1 || $scope.imgValue < 0) {
          $scope.imgValue = 0;
      }

      $scope.image = images[$scope.imgValue];

    });

  });

});
