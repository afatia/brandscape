define(['./module'], function (services) {

  services.factory('imageService', function(Restangular) {

  var portfolioImages = Restangular.one('project', 1).getList('image');

  return {
      getPortfolio: function () {
        return portfolioImages;
      }
  };

  });

});
