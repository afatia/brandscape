define(['./app'], function (app) {
    'use strict';

    return app.config(function ($locationProvider, $stateProvider, $urlRouterProvider, RestangularProvider) {
        // ROUTES AND STATES
        $stateProvider
          // HOME
          .state('home', {
            url: "/home",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'templates/main.html',
                  controller: 'homeController'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                },
            }
            })
            .state('home.construcao', {
              views: {
                "main@home": {
                    templateUrl: 'views/home/home.construcao.html'
                  }
              }
            })
            .state('home.saude', {
              views: {
                "main@home": {
                  templateUrl: 'views/home/home.saude.html'
                }
              }
            })
            .state('home.business', {
              views: {
                "main@home": {
                  templateUrl: 'views/home/home.business.html'
                }
              }
            })
            .state('home.premium', {
              views: {
                "main@home": {
                  templateUrl: 'views/home/home.premium.html'
                }
              }
            })
          // EMPRESA
          .state('empresa', {
            url: "/empresa",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/empresa.html'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          })
          // EMPRESA
          .state('servicos', {
            url: "/servicos",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/servicos.html'
                },
                "content@servicos": {
                    templateUrl: 'views/servicos/servicos.lista.html'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          })
          .state('servicos.construcao', {
              url: "/construcao",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.construcao.html'
                  }
              }
          })
          .state('servicos.arquitectura', {
              url: "/arquitectura",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.arquitectura.html'
              }
            }
          })
          .state('servicos.instalacoes', {
              url: "/instalacoes",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.instalacoes.html'
              }
            }
          })
          .state('servicos.manutencao', {
              url: "/manutencao",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.manutencao.html'
              }
            }
          })
          .state('servicos.reformas', {
              url: "/reformas",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.reformas.html'
              }
            }
          })
          .state('servicos.marcenaria', {
              url: "/marcenaria",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.marcenaria.html'
              }
            }
          })
          .state('servicos.elaboracao', {
              url: "/elaboracao",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.elaboracao.html'
              }
            }
          })
          .state('servicos.actividades', {
              url: "/actividades",
              views: {
                  'content@servicos': {
                    templateUrl: 'views/servicos/servicos.actividades.html'
              }
            }
          })
          // EMPRESA
          .state('equipa', {
            url: "/equipa",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/equipa.html'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          })
          // PORTFOLIO
          .state('portfolio', {
            url: "/portfolio/:imageId",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/portfolio.html',
                  controller: 'portfolioController'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          })
          // CONTACTOS
          .state('contactos', {
            url: "/contactos",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/contactos.html'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          })
          // LOGIN
          .state('login', {
            url: "/login",
            views: {
                "header": {
                  templateUrl: 'templates/navbar.html',
                  controller: 'navbarController'
                },
                "body": {
                  templateUrl: 'views/login.html'
                },
                "footer": {
                  templateUrl: 'templates/footer.html'
                }
            }
          });

          $urlRouterProvider.otherwise('/home');
          $urlRouterProvider.when('/portfolio', '/portfolio/1');
          $locationProvider.html5Mode(true);
          RestangularProvider.setBaseUrl('/api');
          // RestangularProvider.setDefaultHttpFields({cache: true});
    });
});
