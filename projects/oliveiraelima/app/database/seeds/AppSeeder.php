<?php

class AppSeeder extends Seeder {

  public function run() {

    DB::table('users')->delete();
    DB::table('projects')->delete();
    DB::table('images')->delete();
    DB::table('users_projects')->delete();

    // Seed Utilizadores
    $userIataLima = User::create(array(
      'name' => 'Iata',
      'surname' => 'Lima',
      'email' => 'iatalima@oliveiraelima.com',
      'password' => Hash::make('admin'),
      'administrator_privileges' => true
    ));

    $userGuest1 = User::create(array(
      'name' => 'Guest',
      'surname' => '1',
      'email' => 'guest1@oliveiraelima.com',
      'password' => Hash::make('guest'),
      'administrator_privileges' => false
    ));

    $userGuest2 = User::create(array(
      'name' => 'Guest',
      'surname' => '2',
      'email' => 'guest2@oliveiraelima.com',
      'password' => Hash::make('guest'),
      'administrator_privileges' => false
    ));

    $userGuest3 = User::create(array(
      'name' => 'Guest',
      'surname' => '3',
      'email' => 'guest3@oliveiraelima.com',
      'password' => Hash::make('guest'),
      'administrator_privileges' => false
    ));

    $this->command->info('Utilizadores criados com sucesso.');

    // Seed Projectos
    $portfolio = Project::create(array(
      'name' => 'Portfolio',
    ));

    $projectA = Project::create(array(
      'name' => 'Projecto B',
    ));

    $projectB = Project::create(array(
      'name' => 'Projecto X',
    ));

    $projectC = Project::create(array(
      'name' => 'Projecto Y',
    ));


    $this->command->info('Projectos criados com sucesso.');

    // Seed Imagens
    $image1 = Image::create(array(
      'name' => 'imagem1',
      'description' => 'descricao imagem1',
      'url' => '/images/portfolio/business/01.jpg',
      'project_id' => $portfolio->id
    ));

    $image2 = Image::create(array(
      'name' => 'imagem2',
      'description' => 'descricao imagem2',
      'url' => '/images/portfolio/business/02.jpg',
      'project_id' => $portfolio->id
    ));

    $image3 = Image::create(array(
      'name' => 'imagem3',
      'description' => 'descricao imagem3',
      'url' => '/images/saude/01.jpg',
      'project_id' => $projectC->id
    ));

    $image4 = Image::create(array(
      'name' => 'imagem4',
      'description' => 'descricao imagem4',
      'url' => '/images/portfolio/saude/02.jpg',
      'project_id' => $portfolio->id
    ));

    $image5 = Image::create(array(
      'name' => 'imagem5',
      'description' => 'descricao imagem5',
      'url' => '/images/habitacao-premium/01.jpg',
      'project_id' => $projectB->id
    ));

    $image6 = Image::create(array(
      'name' => 'imagem6',
      'description' => 'descricao imagem6',
      'url' => 'images/habitacao-premium/02.jpg',
      'project_id' => $projectA->id
    ));

    $this->command->info('Imagens criadas com sucesso.');

    // Seed Utitilizadores-Projectos
    $userIataLima->projects()->attach($portfolio->id);
    $userGuest1->projects()->attach($projectA->id);

    $userGuest1->projects()->attach($projectB->id);
    $userGuest2->projects()->attach($projectC->id);

    $userGuest3->projects()->attach($projectA->id);
    $userGuest3->projects()->attach($projectB->id);

    $this->command->info('Relações utilizadores-projectos criadas com sucesso.');

  }

}
