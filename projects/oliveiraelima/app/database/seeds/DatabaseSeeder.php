<?php

class DatabaseSeeder extends Seeder {

	public function run()
	{
        Eloquent::unguard();
				$this->call('AppSeeder');
				$this->command->info('Base de dados preenchida.');
	}

}
