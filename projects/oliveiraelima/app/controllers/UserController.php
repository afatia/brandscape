<?php

class UserController extends \BaseController {

	/**
	 * Send back all Users as JSON
	 *
	 * @return Response
	 */
	public function __construct() {

		// $this->beforeFilter('oauth', array('only' => array('index', 'update', 'destroy')));
	}

	public function index()
	{
		return Response::json(array('users' => User::get()));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $regras = array(
      'nome' => 'required',
      'apelido' => 'required',
      'email' => 'required|email',
      'password' => 'required'
    );

    $validar = Validator::make(Input::all(), $regras);

    if ($validar->fails()) {
        return Response::json($validar->failed());
    }
    else {
      // Guardar
      $User = new User;
      $User->nome = Input::get('nome');
      $User->apelido = Input::get('apelido');
      $User->email = Input::get('email');
      $User->password = Input::get('password');
      $User->save();

      return Response::json(array('success' => true));
    }
	}

	/**
	 * Update the specified resource in storage
	 *
	 * @param int $id
	 * @return Response
	*/
	public function update($id)
	{
		$regras = array(
			'nome' => 'required',
			'apelido' => 'required',
			'email' => 'required|email',
			'password' => 'required'
		);

		$validar = Validator::make(Input::all(), $regras);

		if ($validar->fails()) {
				return Response::json($validar->failed());
		}
		else {
			// Guardar
			$User = User::find($id);
			$User->nome = Input::get('nome');
			$User->apelido = Input::get('apelido');
			$User->email = Input::get('email');
			$User->password = Input::get('password');
			$User->save();

			return Response::json(array('success' => true));
		}
	}
	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(array('user' => User::find($id)));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);

		return Response::json(array('success' => true));
	}

}
