<?php

class ProjectController extends \BaseController {

  /**
   * Send back all Projectys as JSON
   *
   * @return Response
   */
  public function __construct() {

    // $this->beforeFilter('oauth', array('only' => array('store', 'update', 'destroy')));
  }

  public function index()
  {
    return Project::get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $regras = array(
      'name' => 'required',
      'description' => 'required',
      'url' => 'required|url',
      'id_project' => 'required'
    );

    $validar = Validator::make(Input::all(), $regras);

    if ($validar->fails()) {
        return Response::json($validar->failed());
    }
    else {
      // Guardar
      $Project = new Project;
      $Project->name = Input::get('name');
      $Project->description = Input::get('description');
      $Project->url = Input::get('url');
      $Project->id_project = Input::get('id_project');
      $Project->save();

      return Response::json(array('success' => true));
    }
  }

  /**
   * Update the specified resource in storage
   *
   * @param int $id
   * @return Response
  */
  public function update($id)
  {
    $regras = array(
      'name' => 'required',
      'description' => 'required',
      'url' => 'required|url',
      'id_project' => 'required'
    );

    $validar = Validator::make(Input::all(), $regras);

    if ($validar->fails()) {
        return Response::json($validar->failed());
    }
    else {
      // Guardar
      $Project = Project::find($id);
      $Project->name = Input::get('name');
      $Project->description = Input::get('description');
      $Project->url = Input::get('url');
      $Project->id_project = Input::get('id_project');
      $Project->save();

      return Response::json(array('success' => true));
    }
  }
  /**
   * Return the specified resource using JSON
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    return Project::find($id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Project::destroy($id);

    return Response::json(array('success' => true));
  }

}
