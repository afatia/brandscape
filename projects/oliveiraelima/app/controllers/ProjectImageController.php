<?php

class ProjectImageController extends \BaseController {

  /**
   * Send back all Users as JSON
   *
   * @return Response
   */
  public function __construct() {

    // $this->beforeFilter('oauth', array('only' => array('index', 'update', 'destroy')));
  }

  public function index($project_id)
  {
    $images = Project::find($project_id)->images()->orderBy('order')->get();

    return $images;
  }

  public function show($project_id, $image_id)
  {
    return Project::find($project_id)->images()->where('id', '=', $image_id)->first();
  }
}
