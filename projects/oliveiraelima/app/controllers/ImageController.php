<?php

class ImageController extends \BaseController {

  /**
   * Send back all Images as JSON
   *
   * @return Response
   */
  public function __construct() {

    // $this->beforeFilter('oauth', array('only' => array('store', 'update', 'destroy')));
  }

  public function index()
  {
    return Image::get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $regras = array(
      'name' => 'required',
      'description' => 'required',
      'url' => 'required|url',
      'id_project' => 'required'
    );

    $validar = Validator::make(Input::all(), $regras);

    if ($validar->fails()) {
        return Response::json($validar->failed());
    }
    else {
      // Guardar
      $Image = new Image;
      $Image->name = Input::get('name');
      $Image->description = Input::get('description');
      $Image->url = Input::get('url');
      $Image->id_project = Input::get('id_project');
      $Image->save();

      return Response::json(array('success' => true));
    }
  }

  /**
   * Update the specified resource in storage
   *
   * @param int $id
   * @return Response
  */
  public function update($id)
  {
    $regras = array(
      'name' => 'required',
      'description' => 'required',
      'url' => 'required|url',
      'id_project' => 'required'
    );

    $validar = Validator::make(Input::all(), $regras);

    if ($validar->fails()) {
        return Response::json($validar->failed());
    }
    else {
      // Guardar
      $Image = Image::find($id);
      $Image->name = Input::get('name');
      $Image->description = Input::get('description');
      $Image->url = Input::get('url');
      $Image->id_project = Input::get('id_project');
      $Image->save();

      return Response::json(array('success' => true));
    }
  }
  /**
   * Return the specified resource using JSON
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    return Image::find($id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Image::destroy($id);

    return Response::json(array('success' => true));
  }

}
