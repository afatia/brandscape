<?php

class UserProjectController extends \BaseController {

  /**
   * Send back all Users as JSON
   *
   * @return Response
   */
  public function __construct() {

    // $this->beforeFilter('oauth', array('only' => array('index', 'update', 'destroy')));
  }
   /**
   * Return the specified resource using JSON
   *
   * @param  int  $id
   * @return Response
   */
  public function index($user_id)
  {
    $projects = User::find($user_id)->projects;

    return Response::json(array("projects" => $projects));
  }

  public function show($user_id, $project_id)
  {
    $project = User::find($user_id)->projects()->where('project_id', '=', $project_id)->first();

    return Response::json(array("project" => $project));
  }
}
