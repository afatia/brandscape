<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function()
{
    // Route to Angular.js
    return View::make('index');
});

// Route Group API
Route::group(array('prefix' => 'api'), function() {

    Route::resource('user', 'UserController',
            array('except' => array('create', 'edit')));

    Route::resource('project', 'ProjectController',
            array('except' => array('create', 'edit')));

    Route::resource('user.project', 'UserProjectController',
            array('except' => array('create', 'edit')));

    Route::resource('project.image', 'ProjectImageController',
            array('except' => array('store', 'update', 'create', 'edit', 'destroy')));

    Route::resource('image', 'ImageController',
            array('except' => array('create', 'edit')));
});


Route::post('oauth/access_token', function()
{
    return AuthorizationServer::performAccessTokenFlow();
});

App::missing(function($exception)
{
  // Route para o index se não for /api
	return View::make('index');
});
