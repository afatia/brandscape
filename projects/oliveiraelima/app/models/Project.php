<?php

class Project extends Eloquent {

  protected $fillable = array('name');

  public function users() {
    return $this->belongsToMany('User', 'users_projects', 'project_id', 'user_id');
  }

  public function images() {
    return $this->hasMany('Image');
  }

  protected $guarded = array('id');
}
