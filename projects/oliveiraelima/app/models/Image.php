<?php

class Image extends Eloquent {

  protected $fillable = array('name', 'description', 'url', 'project_id');

  public function project() {
    return $this->belongsTo('Project');
  }
}
