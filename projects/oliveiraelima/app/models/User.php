<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

  protected $fillable = array('name', 'surname', 'email', 'password');

  public function projects() {
    return $this->belongsToMany('Project', 'users_projects', 'user_id', 'project_id');
  }

	protected $guarded = array('id', 'admin');

	public function getIsAdminAttribute()
	{
    return $this->attributes['administrator_privileges'] == 'true';
	}

	protected $appends = array('is_admin');
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'administrator_privileges');

}
