<!doctype html>
<html lang="pt">
<head>

  <title>Oliveira e Lima - Construções</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <link href="/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <link href="/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
  <!--[if IE]>
      <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <![endif]-->
  <!-- SEO -->
  <meta name="author" content="Brandscape Lifestyle Creativity">
  <meta name="description" content="A OLIVEIRA E LIMA LDA. DESENVOLVE A SUA ATIVIDADE NO MERCADO ANGOLANO, DINAMIZANDO PROJECTOS QUE TÊM COMO OBJECTIVO A EXCELÊNCIA E A VALORIZAÇÃO DAS PESSOAS.">
  <link rel="canonical" href="http://www.oliveiraelima.com/">
  <!-- FACEBOOK -->
  <meta property="og:url" content="http://www.oliveiraelima.com/">
  <meta property="og:type" content="website">
  <meta property="og:title" content="OLIVEIRA E LIMA LDA.">
  <meta property="og:image" content="http://www.oliveiraelima.com/images/logos/ol.png"/>
  <meta property="og:description" content="A OLIVEIRA E LIMA LDA. DESENVOLVE A SUA ATIVIDADE NO MERCADO ANGOLANO, DINAMIZANDO PROJECTOS QUE TÊM COMO OBJECTIVO A EXCELÊNCIA E A VALORIZAÇÃO DAS PESSOAS.">
  <meta property="og:site_name" content="Oliveira e Lima">
  <base href="/">
</head>
<body>

    <header ui-view="header"></header>

      <section ui-view="body"></section>

    <footer ui-view="footer"></footer>

<script src="lib/requirejs/require.js" data-main="js/main.js"></script>
</body>
</html>
